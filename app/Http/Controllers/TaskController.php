<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\TaskRepository;
use Illuminate\Validation\ValidationException;
use Lang;

class TaskController extends Controller
{
    /**
     * @var
     */
    protected $taskRepository;

    /**
     * TaskController constructor.
     *
     */
    public function __construct()
    {
        $this->taskRepository = new TaskRepository();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $user = Auth()->user()->id;
        $tasks = $this->taskRepository->all($user);

        return response()->json(
            [
                'status' => Lang::get('messages.SUCCESS'),
                'data' => $tasks
            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
        ]);

        $user = Auth()->user()->id;
        $task = $request->input('description');

        $res = $this->taskRepository->addTask($task, $user);

        return response()->json(
            [
                'status' => Lang::get('messages.SUCCESS'),
                'data' => $res
            ],
            200
        );
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $user = Auth()->user();
        $response = $this->taskRepository->findTask($id);

        if ($response->user_id == $user->id)
        {
            $this->taskRepository->deleteTask($id);
            return response()->json(
                [
                    'status' => Lang::get('messages.SUCCESS')
                ]
            );
        }
    }
}
