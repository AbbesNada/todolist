<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationFormRequest;
use Lang;

class APIController extends Controller
{
    /**
     * @var
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required|email',
                'password' => 'required|min:6|max:10',
            ]
        );

        $inputs = $request->only('email', 'password');
        $user = $this->userRepository->login($inputs);

        if (!$user) {
            return response()->json(
                [
                    'status' => Lang::get('messages.ERROR'),
                    'message' => Lang::get('messages.INVALID')
                ],
                404);
        }

        $token = $this->userRepository->generateToken($user);

        if (!$token) {
            return response()->json(
                [
                    'status' => Lang::get('messages.ERROR'),
                    'message' => Lang::get('messages.INVALID'),
                ],
                401
            );
        }

        $user->token = $token;

        return response()->json(
            [
                'status' => Lang::get('messages.LOGIN'),
                'data' => $user
            ],
            200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function logout(Request $request)
    {
        $this->validate(
            $request,
            [
                'token' => 'required'
            ]
        );

        try {
            $token = $request->input('token');
            $this->userRepository->invalidateToken($token);

            return response()->json(
                [
                    'success' => Lang::get('messages.SUCCESS'),
                    'message' => Lang::get('messages.LOGOUT')
                ],
                200
            );
        } catch (JWTException $exception) {
            return response()->json(
                [
                    'success' => Lang::get('messages.ERROR'),
                    'message' => Lang::get('messages.INVALID_LOGOUT')
                ],
                500
            );
        }
    }

    /**
     * @param RegistrationFormRequest $request
     * @return JsonResponse
     */
    public function register(RegistrationFormRequest $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = $this->userRepository->register($name, $email, $password);
        $user->token = $this->userRepository->generateToken($user);

        return response()->json(
            [
                'status' => Lang::get('messages.SUCCESS'),
                'data' => $user
            ],
            200
        );
    }
}
