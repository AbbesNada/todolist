<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class UserRepository
 *
 * @package \App\Repositories
 */
class UserRepository
{
    protected $user;

    /**
     * UserController constructor.
     *
     */
    public function __construct()
    {

    }

    /**
     * @inheritDoc
     */
    public function login($inputs)
    {
        $user = User::where('email', $inputs['email'])->firstOrFail();

        if (!Hash::check($inputs['password'], $user->password)) {
            return null;
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function generateToken($user)
    {
        try {
            return JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function invalidateToken($token)
    {
        try {
            return JWTAuth::invalidate($token);
        } catch (JWTException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function register($name, $email, $password)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

        return $user->fresh();
    }

    /**
     * @inheritDoc
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

}
