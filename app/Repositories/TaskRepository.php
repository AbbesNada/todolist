<?php
namespace App\Repositories;
use App\Task;
use Illuminate\Support\Facades\DB;

/**
 * Class TaskRepository
 *
 * @package \App\Repositories
 */
class TaskRepository
{
    /**
     * @inheritDoc
     */
    public function all($id)
    {
        return DB::table('tasks')->where('tasks.user_id', '=', $id)
            ->get();
    }

    /**
     * @inheritDoc
     */
    public function addTask($todo, $id)
    {
        $task = new Task();
        $task->user_id = $id;
        $task->title = $todo;
        $task->description = $todo;
        $task->save();
        return $task;
    }

    /**
     * @inheritDoc
     */
    public function findTask($task_id)
    {
        return Task::findOrFail($task_id);
    }

    /**
     * @inheritDoc
     */
    public function deleteTask($task_id)
    {
        return Task::findOrFail($task_id)->delete();
    }
}
