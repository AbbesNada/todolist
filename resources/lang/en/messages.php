<?php

return [
    /**messages**/
    'LOGIN' => 'success',
    'INVALID' => 'Invalid Email or Password',
    'LOGOUT' => 'User logged out successfully',
    'INVALID_LOGOUT' => 'Sorry, the user cannot be logged out',
    'ERROR' => 'Error',
    'NOT_FOUND' => 'Not Found',
    'SUCCESS' => 'success'
];
