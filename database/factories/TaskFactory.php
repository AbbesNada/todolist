<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Task::class, function (Faker $faker) {

    $usersIDs = DB::table('users')->pluck('id');
    return [
        'user_id' => $faker->randomElement($usersIDs),
        'title' => $faker->text(100)
    ];
});
