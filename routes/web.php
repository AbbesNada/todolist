<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('test/{post}', function ($post) {

        //$name = request('name');
    /**1er méthode **/
        //return $name;
    /**2eme méthode **/
        /* return view('test', [
            'name' => $name
        ]);*/
    /**3er méthode **/

    /*$posts = [
        'my-first-post' => 'hello world',
        'my-second-post' => 'laravel'
    ];

    if(! array_key_exists($post, $posts)) {
        abort(404, 'n existe pas');
    }

    return view ('test', [
        'post' => $posts[$post], //?? 'nothing',
    ]);
});*/

Route::get('test/{post}', 'TestController@show');

Route::get('todo', function (){
    return view('todo');
});

Route::get('task', function (){
    return view('task');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::post('register', 'ApiController@register');
